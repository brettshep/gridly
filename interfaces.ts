import { ValidatorFn } from "@angular/forms";

export interface AsideInfo {
  icon: string;
  title: string;
  description: string;
  colorClass: string;
}

export interface GridImg {
  src: string;
  title: string;
  lastEdit: number;
}

export interface AsideInfo {
  icon: string;
  title: string;
  description: string;
  colorClass: string;
}

export interface GridCategory {
  category: string;
  grids: { name: string; src: string }[];
}

//Grid Settings

export interface Setting {
  displayName: string;
  name: string;
  type: "text" | "number";
  unit: null | "px" | "°";
  children: Setting[];
  errorMsg: string;
  validators?: ValidatorFn[];
}

export interface SettingsGroup {
  name: string;
  settings: Setting[];
}

export interface GridFormSettings {
  [key: string]: SettingsGroup[];
}

//Grid Data
export interface GridData {
  height: number;
  width: number;
  bgColor: string;
  [key: string]: any;
}

export interface PreviewData {
  gridUrl: string;
  bgUrl: string;
}

export interface DownloadSettings {
  resolution: 0.5 | 1 | 2;
  background: boolean;
  format: "png" | "svg";
  name: string;
}

export interface DefaultGridData {
  [key: string]: GridData;
}

//Grid Base Class
export interface GridBaseClass {
  Draw: (
    ctx: CanvasRenderingContext2D,
    sizeMap: SizeMap,
    data: GridData,
    offset: GridOffset
  ) => void;
}

//Offset
export interface GridOffset {
  x: number;
  y: number;
}

//Size Map
export interface SizeMap {
  currWidth: number;
  baseWidth: number;
}

//Dot Grid
export interface DotGridData extends GridData {
  dotRadius: number;
  dotSpacing: number;
}
