import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

export enum CompName {
  home = 1,
  grids,
  editor
}

const routes: Routes = [
  {
    path: "home",
    data: { name: CompName.home },
    loadChildren: "./pages/home-page/home.module#HomeModule"
  },
  {
    path: "grids",
    data: { name: CompName.grids },
    loadChildren: "./pages/grids-page/grids.module#GridsModule"
  },
  {
    path: "editor",
    data: { name: CompName.editor },
    loadChildren: "./pages/editor-page/editor.module#EditorModule"
  },
  { path: "**", redirectTo: "home", pathMatch: "full" }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
