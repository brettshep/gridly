import { Validators } from "@angular/forms";
import { DotGrid } from "./basic/dot-grid";
import { Injectable } from "@angular/core";
import {
  GridCategory,
  GridFormSettings,
  DefaultGridData
} from "../../../interfaces";
import { OnePoint } from "./point-perspective/one-point";
import { GridBaseClass } from "../../../interfaces";

@Injectable({
  providedIn: "root"
})
export class GridsService {
  //grid names
  onePoint: string = "1 Point";
  dotGrid: string = "Dots";

  //grid names and image links
  gridCategories: GridCategory[] = [
    {
      category: "Point Perspective",
      grids: [
        {
          name: this.dotGrid,
          src: "./assets/gridBoxTemp.png"
        },
        {
          name: this.onePoint,
          src: "./assets/gridBoxTemp.png"
        },
        {
          name: this.onePoint,
          src: "./assets/gridBoxTemp.png"
        }
      ]
    },
    {
      category: "Basic Grids",
      grids: [
        {
          name: this.onePoint,
          src: "./assets/gridBoxTemp.png"
        },
        {
          name: this.onePoint,
          src: "./assets/gridBoxTemp.png"
        },
        {
          name: this.onePoint,
          src: "./assets/gridBoxTemp.png"
        },
        {
          name: this.onePoint,
          src: "./assets/gridBoxTemp.png"
        },
        {
          name: this.onePoint,
          src: "./assets/gridBoxTemp.png"
        }
      ]
    },
    {
      category: "3D Grids",
      grids: [
        {
          name: this.onePoint,
          src: "./assets/gridBoxTemp.png"
        },
        {
          name: this.onePoint,
          src: "./assets/gridBoxTemp.png"
        },
        {
          name: this.onePoint,
          src: "./assets/gridBoxTemp.png"
        },
        {
          name: this.onePoint,
          src: "./assets/gridBoxTemp.png"
        }
      ]
    },
    {
      category: "3D Grids",
      grids: [
        {
          name: this.onePoint,
          src: "./assets/gridBoxTemp.png"
        },
        {
          name: this.onePoint,
          src: "./assets/gridBoxTemp.png"
        },
        {
          name: this.onePoint,
          src: "./assets/gridBoxTemp.png"
        },
        {
          name: this.onePoint,
          src: "./assets/gridBoxTemp.png"
        }
      ]
    },
    {
      category: "3D Grids",
      grids: [
        {
          name: this.onePoint,
          src: "./assets/gridBoxTemp.png"
        },
        {
          name: this.onePoint,
          src: "./assets/gridBoxTemp.png"
        },
        {
          name: this.onePoint,
          src: "./assets/gridBoxTemp.png"
        },
        {
          name: this.onePoint,
          src: "./assets/gridBoxTemp.png"
        }
      ]
    }
  ];

  //grid classes ---FIX CAUSE ITS NOT AN INSTANCE
  gridClasses: { [key: string]: any } = {
    [this.onePoint]: OnePoint,
    [this.dotGrid]: DotGrid
  };

  //grid form control settings
  forms: GridFormSettings = {
    [this.onePoint]: [
      {
        name: "Canvas",
        settings: [
          {
            displayName: "Width",
            name: "width",
            type: "number",
            unit: "px",
            children: null,
            errorMsg: "Min: 1, Max: 10000"
          },
          {
            displayName: "Height",
            name: "height",
            type: "number",
            unit: "px",
            children: null,
            errorMsg: "Min: 1, Max: 10000"
          },
          {
            displayName: "Background",
            name: "bgColor",
            type: "text",
            unit: null,
            children: null,
            errorMsg: "Invalid Color"
          }
        ]
      }
    ],
    [this.dotGrid]: [
      {
        name: "Canvas",
        settings: [
          {
            displayName: "Width",
            name: "width",
            type: "number",
            unit: "px",
            children: null,
            errorMsg: "Min: 1, Max: 10000"
          },
          {
            displayName: "Height",
            name: "height",
            type: "number",
            unit: "px",
            children: null,
            errorMsg: "Min: 1, Max: 10000"
          },
          {
            displayName: "Background",
            name: "bgColor",
            type: "text",
            unit: null,
            children: null,
            errorMsg: "Invalid Color"
          }
        ]
      },
      {
        name: "Dots",
        settings: [
          {
            displayName: "Dot Spacing",
            name: "dotSpacing",
            type: "number",
            unit: null,
            children: null,
            errorMsg: "Min: 10, Max: 500",
            validators: [Validators.min(10), Validators.max(500)]
          },
          {
            displayName: "Dot Radius",
            name: "dotRadius",
            type: "number",
            unit: null,
            children: null,
            validators: [Validators.min(1), Validators.max(50)],
            errorMsg: "Min: 1, Max: 50"
          },
          {
            displayName: "Test Parent",
            name: "test",
            type: "number",
            unit: null,

            errorMsg: "Min: 1, Max: 50",
            children: [
              {
                displayName: "Test Child",
                name: "test",
                type: "number",
                unit: null,
                children: null,
                validators: [Validators.min(1), Validators.max(50)],
                errorMsg: "Min: 1, Max: 50"
              }
            ]
          }
        ]
      }
    ]
  };

  //default grid data
  defaults: DefaultGridData = {
    [this.onePoint]: {
      width: 1920,
      height: 1080,
      bgColor: "white",
      vp1: 10,
      vp2: 13
    },
    [this.dotGrid]: {
      width: 1920,
      height: 1080,
      bgColor: "white",
      dotRadius: 5,
      dotSpacing: 8,
      test: 50
    }
  };
}
