import { GridBaseClass } from "../../../../interfaces";
export class OnePoint implements GridBaseClass {
  lineNum = 36;
  sky = false;

  constructor() {}

  Draw(ctx: CanvasRenderingContext2D) {
    //   const offset = _offset ? { ..._offset } : { x: 0, y: 0 };
    //   const ratio = sizeMap.currWidth / sizeMap.baseWidth;
    //   const canvasHypot = Math.sqrt(canvas.width ** 2 + canvas.height ** 2);
    //   const center = {
    //     x: canvas.width / 2,
    //     y: canvas.height / 2
    //   };
    //   offset.x *= ratio;
    //   offset.y *= ratio;
    //   _ctx.lineWidth = 0.5;
    //   _ctx.strokeStyle = "black";
    //   const x = center.x + offset.x;
    //   const y = center.y + offset.y;
    //   for (let i = 0; i < this.lineNum; i++) {
    //     let theta = (((i * 360) / this.lineNum) * Math.PI) / 180;
    //     //check for sky
    //     if (!this.sky && theta > Math.PI) break;
    //     _ctx.beginPath();
    //     _ctx.moveTo(x, y);
    //     _ctx.lineTo(
    //       x + canvasHypot * Math.cos(theta),
    //       y + canvasHypot * Math.sin(theta)
    //     );
    //     _ctx.stroke();
    //   }
    //   //radial gradient
    //   const grd = _ctx.createRadialGradient(x, y, 0, x, y, 30);
    //   grd.addColorStop(0, "rgba(255,255,255,.8)");
    //   grd.addColorStop(1, "rgba(255,255,255,0)");
    //   // draw mask
    //   _ctx.globalCompositeOperation = "destination-out";
    //   _ctx.beginPath();
    //   _ctx.fillStyle = grd;
    //   _ctx.arc(x, y, 30, 0, 2 * Math.PI);
    //   _ctx.fill();
    //   // draw point
    //   _ctx.globalCompositeOperation = "source-over";
    //   _ctx.beginPath();
    //   _ctx.fillStyle = "black";
    //   _ctx.arc(x, y, 3, 0, 2 * Math.PI);
    //   _ctx.fill();
  }

  setLineNum(e) {
    this.lineNum = e;
  }

  setSky(e) {
    this.sky = e;
  }
}
