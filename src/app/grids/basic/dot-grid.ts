import {
  GridBaseClass,
  DotGridData,
  GridOffset,
  SizeMap
} from "../../../../interfaces";
export class DotGrid implements GridBaseClass {
  constructor() {}
  canvasW: number;
  canvasH: number;

  Draw(
    ctx: CanvasRenderingContext2D,
    sizeMap: SizeMap,
    data: DotGridData,
    _offset: GridOffset
  ) {
    //set canvas width and height
    this.canvasH = ctx.canvas.height;
    this.canvasW = ctx.canvas.width;

    //set offset
    const ratio = sizeMap.currWidth / sizeMap.baseWidth;
    let offset = _offset ? { ..._offset } : { x: 0, y: 0 };
    offset.x *= ratio;
    offset.y *= ratio;

    ctx.lineWidth = data.dotRadius;
    ctx.fillStyle = "black";
    ctx.lineCap = "round";
    ctx.setLineDash([0, data.dotSpacing]);

    for (
      let x = (offset.x % data.dotSpacing) - data.dotRadius;
      x <= this.canvasW + data.dotRadius;
      x += data.dotSpacing
    ) {
      const y = (offset.y % data.dotSpacing) - data.dotRadius;
      ctx.beginPath();
      ctx.moveTo(x, y);
      ctx.lineTo(x, this.canvasH + data.dotRadius);
      ctx.stroke();
    }

    ctx.setLineDash([]);
  }
}
