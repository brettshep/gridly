import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-home",
  template: `
    <div class="pageCont">
      <side-bar>
        <home-side-bar [slideNum]="slideNum" [locked]="locked"></home-side-bar>
      </side-bar>
      <home-main
        (slideEmit)="SetSlide($event)"
        (unlockEmit)="Unlock()"
        class="pageMain"
      ></home-main>
    </div>
  `,
  styleUrls: ["./home.component.sass"]
})
export class HomeComponent implements OnInit {
  slideNum = { num: 0 };
  locked = false;

  constructor() {}

  ngOnInit() {}

  SetSlide(num: number) {
    this.locked = true;
    this.slideNum = { num };
  }

  Unlock() {
    this.locked = false;
  }
}
