import { Component, Input, ChangeDetectionStrategy } from "@angular/core";
import { AsideInfo } from "../../../../../../interfaces";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "home-slide",
  template: `
    <div class="cont">
      <!-- Icon -->
      <div [ngClass]="['icon', item.colorClass]">
        <i [class]="item.icon"></i>
      </div>

      <!-- Title -->
      <h2>{{ item.title }}</h2>

      <!-- Description -->
      <p>
        {{ item.description }}
      </p>
    </div>
  `,
  styleUrls: ["./slide.component.sass"]
})
export class HomeSlideComponent {
  @Input() item: AsideInfo;
}
