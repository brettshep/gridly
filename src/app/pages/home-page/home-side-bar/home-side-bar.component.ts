import {
  Component,
  ChangeDetectionStrategy,
  Input,
  ChangeDetectorRef
} from "@angular/core";
import { interval, Subscription } from "rxjs";
import { AsideInfo } from "../../../../../interfaces";
import { FadeInOut } from "src/app/animations";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "home-side-bar",
  template: `
    <div class="container">
      <!-- Pro Logo -->
      <img src="./assets/tempLogoPro.png" alt="proLogo" />

      <!-- Slide -->
      <div class="slideCont" [ngSwitch]="currIndex">
        <home-slide
          *ngSwitchCase="0"
          [item]="items[currIndex]"
          @Fade
        ></home-slide>
        <home-slide
          *ngSwitchCase="1"
          [item]="items[currIndex]"
          @Fade
        ></home-slide>
        <home-slide
          *ngSwitchCase="2"
          [item]="items[currIndex]"
          @Fade
        ></home-slide>
      </div>
      <!-- Dots -->
      <div class="dots">
        <span
          *ngFor="let item of items; index as i"
          (click)="DotClick(i)"
          [class.active]="i === currIndex"
        ></span>
      </div>

      <!-- Buy -->
      <button [class]="items[currIndex].colorClass">Go Pro for $100k</button>

      <!-- Login -->
      <button>Login</button>
    </div>
  `,
  styleUrls: ["./home-side-bar.component.sass"],
  animations: [FadeInOut]
})
export class HomeSideBarComponent {
  constructor(private cd: ChangeDetectorRef) {}

  @Input() set slideNum(num: { num: number }) {
    this.Iterate(num.num);
  }
  @Input() set locked(bool: boolean) {
    if (bool) {
      this.intervalSub.unsubscribe();
    } else {
      if (this.intervalSub) this.intervalSub.unsubscribe();
      this.CreateInterval();
    }
  }

  currIndex = 0;
  items: AsideInfo[] = [
    {
      icon: "i-grid",
      title: "Grids",
      description:
        "Assertively strategize intuitive manufactured products via ROI.",
      colorClass: "blue"
    },
    {
      icon: "i-template",
      title: "Templates",
      description:
        "Use over 180 different pre-made templates organized for each grid to save time when starting out.",
      colorClass: "purple"
    },
    {
      icon: "i-heart",
      title: "Saved",
      description:
        "Collaboratively evolve scalable best practices. Interactively evisculate focused sources.",
      colorClass: "red"
    }
  ];
  itemNameMap = {
    Grids: 0,
    Templates: 1,
    Saved: 2
  };
  intervalSub: Subscription;
  intervalObs = interval(4000);

  ngOnDestroy() {
    this.intervalSub.unsubscribe();
  }

  CreateInterval() {
    this.intervalSub = this.intervalObs.subscribe(() => this.Iterate());
  }

  DotClick(i: number) {
    this.Iterate(i);
    //close sub if one
    this.intervalSub.unsubscribe();
  }

  Iterate(setNum?: number) {
    if (setNum !== undefined) {
      this.currIndex = setNum;
    } else {
      this.currIndex++;
      if (this.currIndex >= this.items.length) this.currIndex = 0;
      else if (this.currIndex < 0) this.currIndex = this.items.length - 1;
    }
    this.cd.markForCheck();
  }
}
