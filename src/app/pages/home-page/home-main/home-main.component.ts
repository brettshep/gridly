import { GridImg } from "../../../../../interfaces";
import {
  Component,
  ChangeDetectionStrategy,
  Output,
  EventEmitter
} from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "home-main",
  template: `
    <main>
      <!-- Logo ---!!!!!!REMOVE CLICK TO TOGGLE LOCKED!!!!-->
      <img
        (click)="locked = !locked"
        class="logo"
        src="./assets/tempLogo.png"
        alt="logo"
      />

      <!-- Tagline -->
      <h3 class="tagline">Get some perspective... fool.</h3>

      <!---- Menu ---->
      <section class="menu">
        <!-- grids -->
        <div class="menuItem">
          <a
            routerLink="/grids"
            class="gridBtn"
            (mouseenter)="changeSlide(0)"
            (mouseleave)="unlockSlide()"
          >
            <i class="i-grid"></i>
          </a>
          <h3>Grids</h3>
        </div>
        <!-- Templates -->
        <div class="menuItem">
          <a
            routerLink="/templates"
            [ngClass]="{
              locked: locked,
              lockedHover: locked,
              templateBtn: true
            }"
            (mouseenter)="changeSlide(1)"
            (mouseleave)="unlockSlide()"
          >
            <i class="i-template"></i>
          </a>
          <h3>Templates</h3>
        </div>
        <!-- Saves -->
        <div class="menuItem">
          <a
            routerLink="/saved"
            [ngClass]="{ locked: locked, lockedHover: locked, savedBtn: true }"
            (mouseenter)="changeSlide(2)"
            (mouseleave)="unlockSlide()"
          >
            <i class="i-heart"></i>
          </a>
          <h3>Saved</h3>
        </div>
        <!-- account -->
        <div *ngIf="!locked" class="menuItem">
          <!-- <button
            [ngClass]="{
              locked: locked,
              lockedHover: locked,
              accountBtn: true
            }"
          > -->
          <a routerLink="/account" class="accountBtn">
            <i class="i-account"></i>
          </a>
          <h3>Account</h3>
        </div>
      </section>
      <!-- Recent -->
      <section class="recent">
        <h1>RECENT</h1>
        <div [ngClass]="{ bg: true, locked: locked }">
          <div class="gridImg" *ngFor="let grid of recent; index as i">
            <figure [class.lockedHover]="locked">
              <img src="{{ locked ? lockedImg : grid.src }}" alt="grid image" />
            </figure>
            <h4>{{ grid.title }}</h4>
            <h5>{{ grid.lastEdit | timeSince }}</h5>
          </div>
        </div>
      </section>
    </main>
  `,
  styleUrls: ["./home-main.component.sass"]
})
export class HomeMainComponent {
  @Output() slideEmit = new EventEmitter();
  @Output() unlockEmit = new EventEmitter();

  locked: boolean = false;
  lockedImg = "./assets/gridBoxLockedTemp.png";
  recent: GridImg[] = [
    {
      title: "3D Room",
      lastEdit: Date.now() - 12145654,
      src: "./assets/gridBoxTemp.png"
    },
    {
      title: "2 Point",
      lastEdit: Date.now() - 1211333,
      src: "./assets/gridBoxTemp.png"
    },
    {
      title: "Hallway",
      lastEdit: Date.now() - 125316541,
      src: "./assets/gridBoxTemp.png"
    }
  ];

  changeSlide(slideNum: number) {
    this.slideEmit.emit(slideNum);
  }
  unlockSlide() {
    this.unlockEmit.emit();
  }
}
