import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "./home.component";
import { HomeMainComponent } from "./home-main/home-main.component";
import { HomeSideBarComponent } from "./home-side-bar/home-side-bar.component";
import { HomeSlideComponent } from "./home-side-bar/slide/slide.component";
import { SideBarModule } from "../../shared/side-bar/side-bar.module";
import { PipesModule } from "../../shared/pipes/pipes.module";

export const ROUTES: Routes = [
  {
    path: "",
    component: HomeComponent
  },
  { path: "**", redirectTo: "/home" }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES),
    SideBarModule,
    PipesModule
  ],
  declarations: [
    HomeComponent,
    HomeMainComponent,
    HomeSlideComponent,
    HomeSideBarComponent
  ]
})
export class HomeModule {}
