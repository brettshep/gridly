import { Component, ChangeDetectionStrategy } from "@angular/core";
import { GridImg } from "../../../../../interfaces";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "grids-side-bar",
  template: `
    <div class="container">
      <div class="gridImg" *ngFor="let grid of recent">
        <figure>
          <img [src]="grid.src" alt="grid image" />
        </figure>
        <h4>{{ grid.title }}</h4>
        <h5>{{ grid.lastEdit | timeSince }}</h5>
      </div>
    </div>
  `,
  styleUrls: ["./grids-side-bar.component.sass"]
})
export class GridsSideBarComponent {
  recent: GridImg[] = [
    {
      title: "3D Room",
      lastEdit: Date.now() - 12145654,
      src: "./assets/gridBoxTemp.png"
    },
    {
      title: "2 Point",
      lastEdit: Date.now() - 1211333,
      src: "./assets/gridBoxTemp.png"
    },
    {
      title: "Hallway",
      lastEdit: Date.now() - 125316541,
      src: "./assets/gridBoxTemp.png"
    },
    {
      title: "Hallway",
      lastEdit: Date.now() - 125316541,
      src: "./assets/gridBoxTemp.png"
    },
    {
      title: "Hallway",
      lastEdit: Date.now() - 125316541,
      src: "./assets/gridBoxTemp.png"
    },
    {
      title: "Hallway",
      lastEdit: Date.now() - 125316541,
      src: "./assets/gridBoxTemp.png"
    }
  ];
}
