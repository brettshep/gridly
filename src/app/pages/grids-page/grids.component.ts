import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-grids",
  template: `
    <div class="pageCont">
      <side-bar [showLogo]="true" [title]="'RECENT'"
        ><grids-side-bar></grids-side-bar
      ></side-bar>
      <grids-main class="pageMain hasMenu"></grids-main>
      <side-menu [setLocked]="false"></side-menu>
    </div>
  `,
  styleUrls: ["./grids.component.sass"]
})
export class GridsComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
