import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { GridsComponent } from "./grids.component";
import { SideBarModule } from "../../shared/side-bar/side-bar.module";
import { GridsMainComponent } from "./grids-main/grids-main.component";
import { GridsSideBarComponent } from "./grids-side-bar/grids-side-bar.component";
import { PipesModule } from "../../shared/pipes/pipes.module";
import { SideMenuModule } from "../../shared/side-menu/side-menu.module";

export const ROUTES: Routes = [
  {
    path: "",
    component: GridsComponent
  },
  { path: "**", redirectTo: "/grids" }
];

@NgModule({
  declarations: [GridsComponent, GridsMainComponent, GridsSideBarComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES),
    SideBarModule,
    PipesModule,
    SideMenuModule
  ]
})
export class GridsModule {}
