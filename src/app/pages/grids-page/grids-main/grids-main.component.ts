import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { GridCategory } from "../../../../../interfaces";
import { GridsService } from "../../../grids/grids.service";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "grids-main",
  template: `
    <main>
      <!-- Grid Container -->
      <div class="container">
        <!-- Grid Category -->
        <section class="category" *ngFor="let cat of grids">
          <!-- Grid Category -->
          <h1>{{ cat.category }}</h1>
          <!-- Grid Row -->
          <div class="gridRow">
            <!-- Grid -->
            <div class="gridImg" *ngFor="let grid of cat.grids">
              <!-- Link -->
              <a [routerLink]="'/editor/' + grid.name">
                <figure>
                  <img [src]="grid.src" alt="grid image" />
                </figure>
              </a>
              <h4>{{ grid.name }}</h4>
            </div>
          </div>
        </section>
      </div>
    </main>
  `,
  styleUrls: ["./grids-main.component.sass"]
})
export class GridsMainComponent implements OnInit {
  grids: GridCategory[];

  constructor(private gridServ: GridsService) {}

  ngOnInit() {
    this.grids = this.gridServ.gridCategories;
  }
}
