import { Injectable, EventEmitter } from "@angular/core";
import { CanvasComponent } from "./editor-main/canvas/canvas.component";
import { PreviewData, DownloadSettings } from "../../../../interfaces";
import * as Saver from "file-saver";
import * as Jszip from "jszip";
@Injectable({
  providedIn: "root"
})
export class DownloadService {
  canvasComp: CanvasComponent;

  //event emitter to triger canvas into settings data
  setCanvasData = new EventEmitter<string>();

  GenerateCanvasPreview(): PreviewData {
    return this.canvasComp.CreatePreview();
  }

  async Download(settings: DownloadSettings) {
    const name = settings.name || "canvas";
    //---filetype is PNG
    if (settings.format === "png") {
      //get canvas blob
      const blob = await this.canvasComp.Download(settings);
      //download
      Saver.saveAs(blob, `${name}.${settings.format}`);
    }

    //---filetype is SVG
    else {
      const blob = this.canvasComp.DownloadSVG(settings);
      const iOS = !!navigator.platform.match(/iPhone|iPod|iPad/);

      //check if on iOS
      if (!iOS) {
        //download
        Saver.saveAs(blob, `${name}.${settings.format}`);
      } else {
        //zip file then save
        const zip = new Jszip();
        zip.file(`${settings.name}.svg`, blob);
        zip.generateAsync({ type: "blob" }).then(blob => {
          Saver.saveAs(blob, `${name}.zip`);
        });
      }
    }
  }
}
