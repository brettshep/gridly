import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { GridsService } from "../../grids/grids.service";
import {
  GridData,
  PreviewData,
  DownloadSettings
} from "../../../../interfaces";
import { DownloadService } from "./download.service";
import { CanvasComponent } from "./editor-main/canvas/canvas.component";
import { GridBaseClass, SettingsGroup } from "../../../../interfaces";

@Component({
  selector: "editor-page",
  template: `
    <div class="pageCont">
      <side-bar [showLogo]="true" [title]="gridName"
        ><editor-side-bar
          [defaultData]="defaultData"
          [formSettings]="formSettings"
          (gridDataEmit)="UpdateGridData($event)"
        ></editor-side-bar
      ></side-bar>
      <editor-main
        class="pageMain hasMenu"
        [gridData]="gridData"
        [grid]="grid"
        (triggerDownload)="TriggerDownloadModal()"
      ></editor-main>
      <side-menu [setLocked]="false"></side-menu>
      <download-modal
        *ngIf="downloadOpen"
        [canvasPreviewData]="canvasPreviewData"
        (close)="downloadOpen = false"
        (download)="Download($event)"
      ></download-modal>
    </div>
  `,
  styleUrls: ["./editor.component.sass"]
})
export class EditorComponent implements OnInit {
  grid: GridBaseClass;
  gridName: string;
  gridData: GridData;
  defaultData: GridData;
  formSettings: SettingsGroup[];
  downloadOpen: boolean = false;
  canvasPreviewData: PreviewData;
  @ViewChild(CanvasComponent) canvasComp;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private gridServ: GridsService,
    private downloadServ: DownloadService
  ) {}

  ngOnInit() {
    //get route param
    const id = this.route.snapshot.params["id"];

    //set grid class;
    this.grid = new (this.gridServ.gridClasses[id] as any)() as GridBaseClass;

    //set grid name
    this.gridName = id;

    //if no grid go to grids page
    if (this.grid) {
      //(sent to sidebar form)
      this.defaultData = { ...this.gridServ.defaults[id] };

      //(sent to canvas directly)
      this.gridData = { ...this.defaultData };

      //set form settigns
      this.formSettings = this.gridServ.forms[this.gridName];
    } else {
      this.router.navigateByUrl("/grids");
    }
  }

  ngAfterViewInit() {
    //create bg img url
  }
  //update grid data form incoming form data
  UpdateGridData(data: GridData) {
    this.gridData = data;
  }

  //trigger download modal
  TriggerDownloadModal() {
    //get canvas preview
    this.canvasPreviewData = this.downloadServ.GenerateCanvasPreview();
    // open modal
    this.downloadOpen = true;
  }

  Download(settings: DownloadSettings) {
    this.downloadServ.Download(settings);
  }
}
