import { PreviewData, DownloadSettings } from "../../../../../interfaces";
import {
  Component,
  ViewChild,
  ElementRef,
  Output,
  EventEmitter,
  Input,
  ChangeDetectionStrategy
} from "@angular/core";

@Component({
  // changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "download-modal",
  template: `
    <div class="container" (click)="BgClose($event)">
      <div class="modal">
        <!-- Canvas Preview -->
        <div class="preview">
          <div class="imgCont">
            <img #gridImage alt="grid preview" />
            <img #bgImage [class.active]="background" alt="bgColor" />
          </div>
          <div class="imageName">
            <div class="inputCont">
              <input #nameInput type="text" placeholder="Untitled" />
              <span></span>
            </div>
          </div>
        </div>
        <!-- Download Settings -->
        <div class="settings">
          <!-- Resolution -->
          <div class="setting thin">
            <h1>Resolution</h1>
            <div class="buttonCont">
              <button
                [class.active]="resolution === 0.5"
                (click)="resolution = 0.5"
              >
                .5x
              </button>
              <button
                [class.active]="resolution === 1"
                (click)="resolution = 1"
              >
                1x
              </button>
              <button
                [class.active]="resolution === 2"
                (click)="resolution = 2"
              >
                2x
              </button>
            </div>
          </div>
          <!-- Background -->
          <div class="setting">
            <h1>Background</h1>
            <div class="buttonCont">
              <button [class.active]="background" (click)="background = true">
                ON
              </button>
              <button [class.active]="!background" (click)="background = false">
                OFF
              </button>
            </div>
          </div>
          <!-- File Format -->
          <div class="setting">
            <h1>File Format</h1>
            <div class="buttonCont">
              <button
                [class.active]="fileType === 'png'"
                (click)="fileType = 'png'"
              >
                PNG
              </button>
              <button
                [class.active]="fileType === 'svg'"
                (click)="fileType = 'svg'"
              >
                SVG
              </button>
            </div>
          </div>
          <!-- Download Button -->
          <div class="downloadBtn">
            <a (click)="Download(nameInput.value)"
              ><button><i class="i-download"></i></button
            ></a>
          </div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ["./download-modal.component.sass"]
})
export class DownloadModalComponent {
  @Input() canvasPreviewData: PreviewData;
  @Output() close = new EventEmitter();
  @Output() download = new EventEmitter<DownloadSettings>();
  @ViewChild("gridImage") gridImageRef: ElementRef;
  @ViewChild("bgImage") bgImageRef: ElementRef;
  resolution: 0.5 | 1 | 2 = 1;
  background = false;
  fileType: "png" | "svg" = "png";

  ngAfterViewInit() {
    //set grid image src
    const gridElem = this.gridImageRef.nativeElement as HTMLImageElement;
    gridElem.src = this.canvasPreviewData.gridUrl;

    //set bg image src
    const bgElem = this.bgImageRef.nativeElement as HTMLImageElement;
    bgElem.src = this.canvasPreviewData.bgUrl;
  }

  BgClose(e: MouseEvent) {
    if ((e.target as HTMLElement).classList.contains("container")) {
      this.close.emit();
    }
  }

  Download(name: string) {
    this.download.emit({
      resolution: this.resolution,
      background: this.background,
      format: this.fileType,
      name
    });
    this.close.emit();
  }
}
