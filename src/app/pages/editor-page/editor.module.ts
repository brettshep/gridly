import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { EditorComponent } from "./editor.component";
import { RouterModule, Routes } from "@angular/router";
import { EditorSideBarComponent } from "./editor-side-bar/editor-side-bar.component";
import { EditorMainComponent } from "./editor-main/editor-main.component";
import { SideBarModule } from "../../shared/side-bar/side-bar.module";
import { SideMenuModule } from "../../shared/side-menu/side-menu.module";
import { CanvasComponent } from "./editor-main/canvas/canvas.component";
import { ReactiveFormsModule } from "@angular/forms";
import { DownloadModalComponent } from "./download-modal/download-modal.component";
import { FormInputComponent } from "./editor-side-bar/form-input/form-input.component";

export const ROUTES: Routes = [
  {
    path: ":id",
    component: EditorComponent
  },
  { path: "**", redirectTo: "/grids" }
];

@NgModule({
  declarations: [
    EditorComponent,
    EditorSideBarComponent,
    EditorMainComponent,
    CanvasComponent,
    DownloadModalComponent,
    FormInputComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES),
    SideBarModule,
    SideMenuModule,
    ReactiveFormsModule
  ]
})
export class EditorModule {}
