import { GridData, GridBaseClass } from "../../../../../interfaces";
import {
  Component,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter
} from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "editor-main",
  template: `
    <main>
      <!-- Canvas Cont -->
      <div class="canvasCont">
        <canvas-comp [setGridData]="gridData" [grid]="grid"></canvas-comp>
      </div>
      <!-- Footer -->
      <footer>
        <div class="proBanner">
          <p>Buy Gridly Pro for $10M</p>
          <button>Buy</button>
          <button>Login</button>
        </div>
        <button class="download" (click)="triggerDownload.emit()">
          <i class="i-download"></i>
        </button>
      </footer>
    </main>
  `,
  styleUrls: ["./editor-main.component.sass"]
})
export class EditorMainComponent {
  @Input() gridData: GridData;
  @Input() grid: GridBaseClass;
  @Output() triggerDownload = new EventEmitter();
}
