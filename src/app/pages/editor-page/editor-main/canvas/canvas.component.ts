import {
  GridData,
  GridBaseClass,
  PreviewData
} from "../../../../../../interfaces";
import { DownloadService } from "../../download.service";
import {
  DownloadSettings,
  SizeMap,
  GridOffset
} from "../../../../../../interfaces";
import {
  Component,
  ChangeDetectionStrategy,
  HostListener,
  ViewChild,
  ElementRef,
  Input
} from "@angular/core";
import * as C2S from "canvas2svg";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "canvas-comp",
  template: `
    <div class="canvasAbs" #canvasWrapper>
      <canvas #canvas></canvas>
    </div>
  `,
  styleUrls: ["./canvas.component.sass"]
})
export class CanvasComponent {
  //canvas and wrapper elementRef
  @ViewChild("canvas") canvasRef: ElementRef;
  @ViewChild("canvasWrapper") canvasWrapperRef: ElementRef;

  //grid data
  gridData: GridData;
  @Input() set setGridData(val: GridData) {
    //assign grid settings
    this.gridData = val;

    //calculate aspect ratio and resize
    if (val.width > 0 && val.height > 0)
      this.aspectRatio = val.width / val.height;

    //check if after ViewInit
    if (this.viewInitComplete) {
      //draw
      this.Draw(true);

      //check resize
      if (val.height !== this.prevCanvasH || val.width !== this.prevCanvasW) {
        this.ResizeCanvas();
        this.prevCanvasH = val.height;
        this.prevCanvasW = val.width;
      }
    } else {
      this.prevCanvasH = val.height;
      this.prevCanvasW = val.width;
    }
  }
  //grid class
  @Input() grid: GridBaseClass;

  //previous canvas height and width
  prevCanvasH: number = 0;
  prevCanvasW: number = 0;

  //view init variable
  viewInitComplete: boolean = false;

  //canvas and wrapper HTML Elems
  canvas: HTMLCanvasElement;
  wrapper: HTMLDivElement;

  //canvas context
  ctx: CanvasRenderingContext2D;

  //ratios and sizemap
  aspectRatio: number = 16 / 9;
  sizeMap: SizeMap = {
    currWidth: null,
    baseWidth: 1000
  };
  canRatio: number;

  //offset
  offset: GridOffset;

  constructor(private downloadServ: DownloadService) {}

  ngOnInit() {
    //set reference to component
    this.downloadServ.canvasComp = this;
  }

  ngOnDestroy() {
    //destroy reference to component
    this.downloadServ.canvasComp = null;
  }

  ngAfterViewInit() {
    //set view success
    this.viewInitComplete = true;

    //set canvas element
    this.canvas = this.canvasRef.nativeElement;

    //set wrapper
    this.wrapper = this.canvasWrapperRef.nativeElement;

    //set canvas context
    this.ctx = this.canvas.getContext("2d");

    //set canvas screen pixel ratio
    const dpr = window.devicePixelRatio || 1,
      bsr =
        (this.ctx as any).webkitBackingStorePixelRatio ||
        (this.ctx as any).mozBackingStorePixelRatio ||
        (this.ctx as any).msBackingStorePixelRatio ||
        (this.ctx as any).oBackingStorePixelRatio ||
        (this.ctx as any).backingStorePixelRatio ||
        1;
    this.canRatio = dpr / bsr;

    //resize canvas
    this.ResizeCanvas();
  }

  //--------DRAW----------
  Draw(drawBG: boolean) {
    //background
    if (drawBG) {
      this.ctx.fillStyle = this.gridData.bgColor;
      this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
    }

    //grid
    this.grid.Draw(this.ctx, this.sizeMap, this.gridData, this.offset);
  }

  CreatePreview(): PreviewData {
    //create temp canvas
    const canvas: HTMLCanvasElement = document.createElement("canvas");

    //set canvas dimensions
    canvas.height = this.canvas.height;
    canvas.width = this.canvas.width;

    //get context
    const ctx = canvas.getContext("2d");

    //draw grid only (no BG)
    this.grid.Draw(ctx, this.sizeMap, this.gridData, this.offset);

    //set grid dataurl
    const gridUrl = canvas.toDataURL();

    //background
    ctx.clearRect(0, 0, canvas.height, canvas.width);
    ctx.fillStyle = this.gridData.bgColor;
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    const bgUrl = canvas.toDataURL();

    //return data url
    return {
      gridUrl,
      bgUrl
    };
  }

  Download(settings: DownloadSettings): Promise<Blob> {
    //create temp canvas
    const canvas: HTMLCanvasElement = document.createElement("canvas");

    //set canvas dimensions
    canvas.height = this.gridData.height * settings.resolution;
    canvas.width = this.gridData.width * settings.resolution;

    //get context
    const ctx = canvas.getContext("2d");

    //check for bg and draw accordingly
    if (settings.background) {
      ctx.fillStyle = this.gridData.bgColor;
      ctx.fillRect(0, 0, canvas.width, canvas.height);
    }

    //draw grid
    this.grid.Draw(ctx, this.sizeMap, this.gridData, this.offset);

    //if dataURL
    return new Promise((res, rej) => {
      canvas.toBlob(blob => {
        res(blob);
      });
    });
  }

  DownloadSVG(settings: DownloadSettings): Blob {
    //create canvas and set resolution
    const width = this.gridData.width * settings.resolution;
    const height = this.gridData.height * settings.resolution;
    const svgCTX = new C2S(width, height);

    //clipping mask
    // svgCTX.rect(0, 0, width, height);
    // svgCTX.clip();

    //draw grid
    if (settings.background) {
      svgCTX.fillStyle = this.gridData.bgColor;
      svgCTX.fillRect(0, 0, width, height);
    }
    this.grid.Draw(svgCTX, this.sizeMap, this.gridData, this.offset);

    //get svg data
    const svgData = svgCTX.getSerializedSvg(true);

    //turn into blob
    return new Blob([svgData], { type: "image/svg+xml" });
  }

  //#region --------RESIZE-------------
  @HostListener("window:resize")
  ResizeCanvas() {
    //get initial height
    const computedStyle = getComputedStyle(this.wrapper);

    const maxHeight =
      this.wrapper.clientHeight -
      (parseFloat(computedStyle.paddingTop) +
        parseFloat(computedStyle.paddingBottom));

    const maxWidth =
      this.wrapper.clientWidth -
      (parseFloat(computedStyle.paddingLeft) +
        parseFloat(computedStyle.paddingRight));

    let height = maxHeight;

    let width = height * this.aspectRatio;
    if (width > maxWidth) {
      width = maxWidth;
      height = width / this.aspectRatio;
    }

    //set canvas elem width and height
    this.canvas.height = height * this.canRatio;
    this.canvas.width = width * this.canRatio;

    //set css width
    this.canvas.style.height = `${height}px`;
    this.canvas.style.width = `${width}px`;

    //set size map currwidth
    this.sizeMap.currWidth = this.canvas.width;

    //draw
    this.Draw(true);
  }
}
