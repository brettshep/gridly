import {
  Component,
  Input,
  ChangeDetectionStrategy,
  ViewChild,
  ElementRef
} from "@angular/core";
import { FormGroup, FormControl, AbstractControl } from "@angular/forms";
import { Setting } from "../../../../../../interfaces";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "form-input",
  template: `
    <div class="container" [formGroup]="formgroup">
      <div class="setting">
        <label>{{ setting.displayName }}</label>

        <!-- NO CHILDREN -->
        <ng-container *ngIf="!setting.children; else hasChildren">
          <div class="inputWrapper">
            <input
              *ngIf="setting.type === 'number'"
              type="number"
              [id]="setting.name"
              [formControlName]="setting.name"
            />
            <input
              *ngIf="setting.type === 'text'"
              type="text"
              [id]="setting.name"
              [formControlName]="setting.name"
            />
            <span>{{ setting.unit }}</span>
          </div>
          <div class="error" *ngIf="formgroup.controls[setting.name].invalid">
            {{ setting.errorMsg }}
          </div>
        </ng-container>

        <!-- WITH CHILDREN -->
        <ng-template #hasChildren>
          <div class="inputWrapper" [class.open]="open">
            <input
              *ngIf="setting.type === 'number'"
              type="number"
              #parentInput
              (input)="SetChildrenInput($event)"
            />
            <input
              *ngIf="setting.type === 'text'"
              type="text"
              #parentInput
              (input)="SetChildrenInput($event)"
            />
            <span class="hasChildren" (click)="ToggleOpen()"
              ><i class="i-down"></i
            ></span>
          </div>
          <div class="error" *ngIf="childError">
            {{ setting.errorMsg }}
          </div>
        </ng-template>
      </div>
      <!-- Children -->
      <div class="childrenCont" *ngIf="setting.children && open">
        <div class="setting" *ngFor="let child of setting.children">
          <label>{{ child.name }}</label>
          <div class="inputWrapper">
            <input
              *ngIf="child.type === 'number'"
              type="number"
              [id]="child.name"
              [formControlName]="child.name"
            />
            <input
              *ngIf="child.type === 'text'"
              type="text"
              [id]="child.name"
              [formControlName]="child.name"
            />
            <span>{{ child.unit }}</span>
          </div>
          <div class="error" *ngIf="formgroup.controls[child.name].invalid">
            {{ child.errorMsg }}
          </div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ["./form-input.component.sass"]
})
export class FormInputComponent {
  @ViewChild("parentInput") parentInputRef: ElementRef;
  @Input() formgroup: FormGroup;
  @Input() setting: Setting;
  open: boolean = false;
  prevValue: any;
  childError: boolean = false;
  ngOnInit() {
    if (this.setting.children) {
      //get child form values
      const childrenVals = this.setting.children.map(val => {
        //return control value
        return this.formgroup.controls[val.name].value;
      });

      //see if all values are same
      const equal = childrenVals.every(v => {
        return v === childrenVals[0];
      });

      //open if values are not equal
      if (!equal) {
        this.open = true;
      }

      //set parent to first childs value regardless
      this.prevValue = childrenVals[0];
    }
  }

  ngAfterViewInit() {
    if (this.setting.children) {
      //grab parent input element and set
      const parentElem = this.parentInputRef.nativeElement as HTMLInputElement;
      parentElem.value = this.prevValue;
    }
  }

  SetChildrenInput(e: Event) {
    //loop through children and set value to parent
    let validCheck = false;
    this.setting.children.forEach(child => {
      //set form value
      const control = this.formgroup.controls[child.name];
      control.setValue((e.target as HTMLInputElement).value);
      //set valid bool
      if (control.invalid) {
        validCheck = true;
      }
    });

    this.childError = validCheck;
  }

  ToggleOpen() {
    //closing
    if (this.open) {
      this.open = false;
      //loop through children and set to parents old value
      this.setting.children.forEach(child => {
        this.formgroup.controls[child.name].setValue(this.prevValue);
      });
      //close parent errors
      this.childError = false;

      //check children for errors and set parent error
      let validCheck = false;
      this.setting.children.forEach(child => {
        //set form value
        const control = this.formgroup.controls[child.name];
        if (control.invalid) {
          validCheck = true;
        }
        this.childError = validCheck;
      });
    }
    //opening
    else {
      this.open = true;
      const parentElem = this.parentInputRef.nativeElement as HTMLInputElement;
      this.prevValue = parentElem.value;

      //close parent errors
      this.childError = false;
    }
  }
}
