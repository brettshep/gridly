import {
  Component,
  ChangeDetectionStrategy,
  Output,
  Input,
  EventEmitter
} from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";
import { Subscription } from "rxjs";
import { GridData, SettingsGroup } from "../../../../../interfaces";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "editor-side-bar",
  template: `
    <form class="container" [formGroup]="form">
      <!-- Settings Group -->
      <section class="settingsGroup" *ngFor="let group of formSettings">
        <h1>{{ group.name }}</h1>
        <!-- Setting -->
        <form-input
          *ngFor="let setting of group.settings"
          [setting]="setting"
          [formgroup]="form"
        >
        </form-input>
      </section>
    </form>
  `,
  styleUrls: ["./editor-side-bar.component.sass"]
})
export class EditorSideBarComponent {
  @Output() gridDataEmit = new EventEmitter<GridData>();
  @Input() defaultData: GridData;
  @Input() formSettings: SettingsGroup[];

  form: FormGroup;
  formSub: Subscription;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    //form
    this.form = this.CreateFormWithDefaults();
    //setup form emit on value change
    this.formSub = this.form.valueChanges.subscribe((val: GridData) => {
      if (this.form.valid) {
        this.gridDataEmit.emit({ ...this.form.value });
      }
    });
  }

  ngOnDestroy() {
    //undub form valuechanges
    if (this.formSub) {
      this.formSub.unsubscribe();
    }
  }

  CreateFormWithDefaults(): FormGroup {
    let group: any = {};
    this.formSettings.forEach(section => {
      section.settings.forEach(setting => {
        //inital required validator

        //if no children
        if (!setting.children) {
          let validators = [Validators.required];
          if (setting.validators) {
            validators = [...validators, ...setting.validators];
          }
          group[setting.name] = new FormControl(
            this.defaultData[setting.name],
            validators
          );
        }
        //has children
        else {
          setting.children.forEach(childSetting => {
            let validators = [Validators.required];
            if (childSetting.validators) {
              validators = [...validators, ...childSetting.validators];
            }
            group[childSetting.name] = new FormControl(
              this.defaultData[childSetting.name],
              validators
            );
          });
        }
      });
    });
    return new FormGroup(group);
  }
}

// {
//   width: ["", [Validators.pattern(/^[1-9][0-9]*$/)]],
//   height: ["", [Validators.pattern(/^[1-9][0-9]*$/)]]
// }

// Validators.pattern(/^[1-9][0-9]*$/)
