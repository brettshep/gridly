import { Component } from "@angular/core";
import {
  RouterOutlet,
  Router,
  NavigationEnd,
  ActivatedRoute,
  ActivationStart
} from "@angular/router";
import { RouteAnimations } from "./animations";
import { CompName } from "./routing.module";
import { filter } from "rxjs/operators";

@Component({
  selector: "app-root",
  template: `
    <div class="page" [@routeAnimation]="routeName">
      <router-outlet #myOutlet="outlet"></router-outlet>
    </div>
  `,
  styleUrls: ["./app.component.sass"],
  animations: RouteAnimations
})
export class AppComponent {
  routeName: number;
  constructor(private router: Router) {
    this.router.events
      .pipe(filter(event => event instanceof ActivationStart))
      .subscribe(event => {
        this.routeName = event["snapshot"].data["name"];
      });
  }

  ngOnInit() {
    // window.addEventListener("touchmove", e => {
    //   e.preventDefault();
    //   e.stopPropagation();
    // });
  }

  //-------ROUTER ANIMS------
  // getDepth(outlet: RouterOutlet) {
  //   let name = outlet.activatedRouteData["name"];
  //   if (name) return name;
  //   else return -1;
  // }
}
