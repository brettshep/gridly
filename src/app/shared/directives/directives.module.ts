import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ClickToggleClassDirective } from "./click-toggle-class.directive";

@NgModule({
  declarations: [ClickToggleClassDirective],
  imports: [CommonModule],
  exports: [ClickToggleClassDirective]
})
export class DirectivesModule {}
