import { Directive, Input, ElementRef, HostListener } from "@angular/core";

@Directive({
  selector: "[clickToggleClass]"
})
export class ClickToggleClassDirective {
  @Input() className: string;
  @Input() classState: boolean;
  constructor(private elemRef: ElementRef) {
    if (this.classState) {
      const elem = this.elemRef.nativeElement as HTMLElement;
      elem.classList.add(this.className);
    }
  }

  @HostListener("click")
  ToggleClass() {
    const elem = this.elemRef.nativeElement as HTMLElement;
    if (this.classState) {
      this.classState = false;
      elem.classList.remove(this.className);
    } else {
      this.classState = true;
      elem.classList.add(this.className);
    }
  }
}
