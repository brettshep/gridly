import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "timeSince"
})
export class TimeSincePipe implements PipeTransform {
  transform(time: number): string {
    const between = ~~(new Date().getTime() - time) / 1000;
    if (between < 3600) {
      return this.pluralize(~~(between / 60), " minute");
    } else if (between < 86400) {
      return this.pluralize(~~(between / 3600), " hour");
    } else {
      return this.pluralize(~~(between / 86400), " day");
    }
  }

  pluralize(time: number, label: string) {
    if (time === 1) {
      return time + label;
    }
    return time + label + "s";
  }
}
