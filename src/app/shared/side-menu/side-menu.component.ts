import { Component, ChangeDetectionStrategy, Input } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "side-menu",
  template: `
    <aside>
      <!-- grids -->

      <a
        (click)="goToURL('/grids')"
        [ngClass]="{
          grids: true,
          active: activeURL === 'grids'
        }"
      >
        <span class="a-content">
          <i class="i-grid"></i>
          <p>Grids</p>
        </span>
      </a>

      <!-- Templates -->
      <a
        (click)="goToURL('/templates')"
        [ngClass]="{
          locked: locked,
          lockedHover: locked,
          templates: true,
          active: activeURL === 'templates' && !locked
        }"
      >
        <span class="a-content">
          <i class="i-template"></i>
          <p>Templates</p>
        </span>
      </a>

      <!-- Saved -->
      <a
        (click)="goToURL('/saved')"
        [ngClass]="{
          locked: locked,
          lockedHover: locked,
          saved: true,
          active: activeURL === 'saved' && !locked
        }"
      >
        <span class="a-content">
          <i class="i-heart"></i>
          <p>Saved</p>
        </span>
      </a>

      <!-- account -->
      <a
        *ngIf="!locked"
        (click)="goToURL('/account')"
        [ngClass]="{
          account: true,
          active: activeURL === 'account' && !locked
        }"
      >
        <span class="a-content">
          <i class="i-account"></i>
          <p>Account</p>
        </span>
      </a>
    </aside>
  `,
  styleUrls: ["./side-menu.component.sass"]
})
export class SideMenuComponent {
  @Input() set setLocked(val: boolean) {
    this.locked = val;
    this.lockedHover = val;
  }
  locked: boolean;
  lockedHover: boolean;
  activeURL: string;

  constructor(private router: Router) {}

  ngOnInit() {
    this.activeURL = this.router.url.split("/")[1];
  }

  goToURL(url: string) {
    this.router.navigateByUrl(url);
  }
}
