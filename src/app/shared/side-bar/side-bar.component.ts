import { Component, ChangeDetectionStrategy, Input } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "side-bar",
  template: `
    <div class="offset"></div>
    <aside>
      <div class="logo">
        <a (click)="goHome()"
          ><img *ngIf="showLogo" src="./assets/tempG.png" alt="logo"
        /></a>
      </div>
      <h1 class="title" *ngIf="title">{{ title }}</h1>
      <ng-content></ng-content>
    </aside>
  `,
  styleUrls: ["./side-bar.component.sass"]
})
export class SideBarComponent {
  @Input() showLogo: boolean;
  @Input() title: boolean;

  constructor(private router: Router) {}

  goHome() {
    this.router.navigateByUrl("/home");
  }
}
